//Import style and component
import "../App.css";
import { getMovieList, searchMovie } from "../api";
import { useEffect, useState } from "react";
import { Navbar } from "./Navbar";
import { Header } from "./Header";
import { TheFooter } from "./TheFooter";
import { AiFillPlayCircle } from "react-icons/ai";
import { ResultTitle } from "./ResultTitle";
import { Link } from "react-router-dom";
import { MdOutlineStarRate } from "react-icons/md";

//Function App
const Home = () => {
  //Initial state of resultTitle
  const [resultTitle, setResultTitle] = useState("Popular Movies");
  //Initial state of popularMovies
  const [popularMovies, setPopularMovies] = useState([]);

  useEffect(() => {
    getMovieList().then((result) => {
      setPopularMovies(result);
    });
  }, []);

  console.log(popularMovies);

  //View list of movies
  const PopularMovieList = () => {
    return popularMovies.map((movie, i) => {
      return (
        //Link to details component
        <Link to={`details/${movie.id}`} style={{ textDecoration: "none" }}>
          <div className="Movie-wrapper" key={i}>
            <div className="Movie-title">
              <p>{movie.title}</p>
            </div>
            <img
              className="Movie-image"
              src={`${process.env.REACT_APP_BASEIMGURL}/${movie.poster_path}`}
              width="300"
            />
            <div className="Movie-rate">
              {`Rate : ${Math.round(movie.vote_average)}`}
              <MdOutlineStarRate
                className=""
                style={{ fontSize: "25px", color: "yellow" }}
              />
            </div>
            <div className="container-watch-card">
              <button className="bt-watch-card">
                <div className="icon-play">
                  <AiFillPlayCircle />
                </div>
                <p>Watch Trailer</p>
              </button>
            </div>
          </div>
        </Link>
      );
    });
  };

  //Functional of search
  const search = async (q) => {
    if (q.length > 3) {
      const query = await searchMovie(q);
      setPopularMovies(query.results);
      setResultTitle(`Search Result "${q}"`);
    } else if (q.length < 3) {
      setResultTitle("Popular Movies");
    }
  };
  return (
    //View of all components
    <div className="App">
      <Navbar onSearch={search} />
      <Header />
      <ResultTitle onResult={resultTitle} />
      <div className="Movie-container">
        <PopularMovieList />
      </div>
      <TheFooter />
    </div>
  );
};

export default Home;
