//Import
import React from "react";
import { detailsMovie } from "../api";
import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { NavbarDetail } from "./NavbarDetail";
import { AiFillPlayCircle } from "react-icons/ai";
import { MdOutlineStarRate } from "react-icons/md";
import { TheFooter } from "./TheFooter";

export const Details = () => {
  //Initial value of state
  const [details, setDetails] = useState("");

  //Get the id from parameters
  const { id } = useParams();

  useEffect(() => {
    detailsMovie(id).then((result) => {
      setDetails(result);
    });
  }, []);

  return (
    <div className="Details">
      {/* View of details component */}
      <NavbarDetail />
      <div id="carouselExampleIndicators" className="carousel slide">
        <div className="carousel-inner">
          <div className="carousel-item-detail active">
            <div className="container-slide-detail">
              <h1>{details.title}</h1>
              <p>{details.overview}</p>
              <div className="Movie-rate">
                <MdOutlineStarRate
                  className=""
                  style={{ fontSize: "25px", color: "yellow" }}
                />
                {Math.round(details.vote_average)}
              </div>
              <button className="bt-watch-detail">
                <div className="icon-play">
                  <AiFillPlayCircle />
                </div>
                <p>Watch Trailer</p>
              </button>
            </div>
            <img
              src={`${process.env.REACT_APP_BASEIMGURL}/${details.poster_path}`}
              className="d-block w-100"
            />
          </div>
        </div>
      </div>
      <TheFooter />
    </div>
  );
};
