//Import style and components
import "../App.css";
import React from "react";
import { AiOutlineSearch } from "react-icons/ai";
import { Link } from "react-router-dom";

export const NavbarDetail = () => {
  return (
    //View of the navbar
    <nav className="navbar-top">
      <h1 className="text-title">Movielist</h1>
      {/* Button login and register */}
      <div className="btn-container">
        <Link to="/" style={{ textDecoration: "none" }}>
          <AiOutlineSearch
            className="mt-3 me-3"
            style={{ fontSize: "35px", color: "black" }}
          />
        </Link>
        <button className="bt-login">Login</button>
        <button className="bt-register">Register</button>
      </div>
    </nav>
  );
};
