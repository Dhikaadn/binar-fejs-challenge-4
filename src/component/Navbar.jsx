//Import style and components
import "../App.css";
import React from "react";
import { AiOutlineSearch } from "react-icons/ai";

export const Navbar = ({ onSearch }) => {
  return (
    //View of the navbar
    <nav className="navbar-top">
      <h1 className="text-title">Movielist</h1>
      {/* Field of search */}
      <div className="container-search">
        <input
          placeholder="Cari film"
          className="Movie-search"
          onChange={({ target }) => onSearch(target.value)}
        />
        <AiOutlineSearch className="mt-2 me-3" style={{ fontSize: "35px" }} />
      </div>
      {/* Button login and register */}
      <div className="btn-container">
        <button className="bt-login">Login</button>
        <button className="bt-register">Register</button>
      </div>
    </nav>
  );
};
